package gui;

import java.io.FileNotFoundException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;

public class Handlers {
	// UI elements assigned to variabes to manipulate them later
	@FXML
	private Label turnLbl;
	@FXML
	private Label rollLbl;
	@FXML
	private TextArea outText;
	@FXML
	private Button rollBtn;
	@FXML
	private Button saveBtn;

	// Initialize game engine
	engine.Game engine = new engine.Game();

	// Create a dice value that equals the die from the engine package
	int dice = engine.dice();

	/**
	 * Adds ladder & snake objects, sets up the text box to be an output only.
	 */
	public void setUp() {
		engine.addObjects();
		outText.setWrapText(true);
		outText.setEditable(false);
	}

	/**
	 * Converts the given cursor coordinates into a tile number.
	 * 
	 * The board tiles increment differently to the canvas' cells, hence the calculation to convert it. 
	 * 
	 * @param row
	 * @param column
	 * @return tile
	 */
	public String findTile(int row, int column) {
		if ((row % 2) == 0) {
			return Integer.toString((10 - row) * 10 + column);
		} else {
			return Integer.toString((10 - row) * 10 + 10 - column + 1);
		}
	}

	/**
	 * Method assigned to save button - saves the game.
	 * 
	 * @param ev
	 */
	public void saveAction(ActionEvent ev) {
		try {
			engine.save();
			outText.appendText("\n Saved game \n\n");
		} catch (FileNotFoundException e) {
			outText.appendText("\n ERROR: \n File not found, check permissions and path: " + e.getMessage() + "\n");
		}
	}

	/**
	 * Method assigned to load button - loads the game.
	 * 
	 * @param ev
	 */
	public void loadAction(ActionEvent ev) {
		try {
			engine.load();
			outText.appendText("\n Loaded savegame \n\n");
			rollBtn.setDisable(false);
			saveBtn.setDisable(false);
			rollLbl.setText("Roll: " + Integer.toString(dice));
			turnLbl.setText("Player " + engine.turn + "'s Turn");
			outText.appendText("\nit is Player " + engine.turn + "'s Turn\n");
			outText.appendText("Player " + engine.turn + " is on tile " + engine.pos + "\n\n");
		} catch (FileNotFoundException e) {
			outText.appendText("\n ERROR: \n File not found, check permissions and path: " + e.getMessage() + "\n");
		}
	}

	/**
	 * Rolls the die when button is clicked.
	 * 
	 * @param ev
	 */
	public void rollAction(ActionEvent ev) {
		dice = engine.dice();
		rollLbl.setText("Roll: " + Integer.toString(dice));
		outText.appendText("Player " + engine.turn + " rolls a " + Integer.toString(dice) + "\n");
	}

	/**
	 * resets the game board when the button is clicked.
	 * 
	 * @param ev
	 */
	public void resetBtn(ActionEvent ev) {
		engine.reset();
		outText.appendText("\nGame reset\n");
		rollBtn.setDisable(false);
		saveBtn.setDisable(false);
		rollLbl.setText("Let's Roll!");
		turnLbl.setText("Player " + engine.turn + "'s Turn");
		outText.setText(
				"\nWelcome to snakes and ladders!\n\nBoth players start on tile 1, your goal is to reach tile 100!\n\nLanding on a ladder will take you higher, but watch out for snakes! if they eat you, you will go backwards!\n");
		outText.appendText("\nit is Player " + engine.turn + "'s Turn\n");
		outText.appendText("Player " + engine.turn + " is on tile " + engine.pos + "\n\n");
	}

	/**
	 * Attempts to move player position to tile clicked.
	 * 
	 * Takes the cursor coordinates, converts them into a tile number using
	 * findTile method, then attempts to move using the engine package's move
	 * method.
	 * 
	 * @param ev
	 */
	public void gridClick(MouseEvent ev) {
		Canvas canvas = (Canvas) ev.getSource();
		int row = (int) (10 * ev.getY() / canvas.getHeight() + 1);
		int column = (int) (10 * ev.getX() / canvas.getWidth() + 1);

		int moveResult = engine.move(findTile(row, column));
		String newLine = "\n";

		if (moveResult == 3) {
			outText.appendText(newLine + "You must move to the tile that equals your tile + the dice " + "( "
					+ engine.pos + " + " + engine.dice + " = " + "?" + " )" + newLine);
		} else if (moveResult == 2) {
			outText.appendText("You must roll the dice first!" + newLine);
		} else if (moveResult == 1) {
			if (engine.checkTile()) {
				outText.appendText(
						"You hit a " + engine.tempBo.getType() + " and go to tile " + engine.tempBo.getEnd() + "!\n");
			} else {
				outText.appendText("Player " + engine.turn + " moves to tile " + engine.pos + newLine);
			}
			engine.setTurn();
			if (engine.winner() != 0) {
				outText.appendText(newLine + "Player " + engine.winner() + " has reached tile 100 and wins!" + newLine
						+ "\nGame Over");
				rollLbl.setText("Game Over");
				turnLbl.setText("Payer " + engine.winner() + " wins!");
				rollBtn.setDisable(true);
				saveBtn.setDisable(true);
			} else {
				rollLbl.setText("Let's Roll");
				turnLbl.setText("Player " + engine.turn + "'s Turn");
				outText.appendText("\nit is Player " + engine.turn + "'s Turn\n");
				outText.appendText("Player " + engine.turn + " is on tile " + engine.pos + "\n\n");
			}

		} else if (moveResult == 0) {
			outText.appendText("\n\nUNKNOWN ERROR: ERROR CODE: 01 - PLEASE RESET\n\n");
		}

	}

}
