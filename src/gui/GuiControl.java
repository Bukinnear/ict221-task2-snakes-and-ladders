package gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.stage.Stage;

/**
 * Class to setup & start the GUI
 * @author Jared
 *
 */

public class GuiControl extends Application {

	public void start(Stage stage) throws Exception {

		FXMLLoader fxmlLoader = new FXMLLoader();
		SplitPane root = (SplitPane) fxmlLoader.load(getClass().getResource("FxmlGui.fxml").openStream());
		Handlers handlers = (Handlers) fxmlLoader.getController(); 

		Scene scene = new Scene(root, 800, 600);

		stage.setTitle("Snakes and Ladders");
		stage.setScene(scene);
		stage.setResizable(false);
		handlers.setUp();
		stage.show();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}

}
