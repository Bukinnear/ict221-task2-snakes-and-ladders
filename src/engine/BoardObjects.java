package engine;

/**
 * this class is the super constructor for, and includes, the snake and ladder 
 * classes as nested classes. All snakes and ladders have a start point, an end 
 * point and a type as a string to identify them.
 * 
 * @author Jared Kinnear
 */

public class BoardObjects {
	
	// public variables for subclasses
	public int start = 0;
	public int end = 0;
	public String type = "";

	/**
	 * method to return start of an object.
	 * 
	 * @return start
	 */
	public int getStart() {
		return start;
	}

	/**
	 * method to return end of an object.
	 * 
	 * @return end
	 */
	public int getEnd() {
		return end;
	}

	/**
	 * Returns the type of object (snake/ladder).
	 * 
	 * @return type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 *  Glorified constructor for ladder objects
	 *  
	 *  This could have just been a board object constructor, but apparently 
	 *  this assignment needed inheritance sooooooooo here it is.
	 *  
	 * @author Jared
	 *
	 */
	public static class Ladder extends BoardObjects {
		
		public Ladder(int s, int e) {
			type = "ladder";
			start = s;
			end = e;
		}
	}
	
	/**
	 *  Glorified constructor for snake objects
	 *  
	 *  This could have just been a board object constructor, but apparently 
	 *  this assignment needed inheritance sooooooooo here it is.
	 *  
	 * @author Jared
	 *
	 */
	public static class Snake extends BoardObjects {

		public Snake(int s, int e) {
			type = "snake";
			start = s;
			end = e;
		}

	}

}


