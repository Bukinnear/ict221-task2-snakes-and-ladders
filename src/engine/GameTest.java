package engine;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;

public class GameTest {

	protected Game game;

	@Before
	public void setup() {
		// basic game setup.
		game = new Game();
		game.addObjects();
	}

	@Test
	public void testAddObjects() {
		// I'm not sure how to test objects without using the methods to 
		// manipulate them. I assume this is the wrong thing to do, so I
		// am trying to avoid it where possible.
		
		// clear list then add them back and count how many there are
		game.boardList.clear();
		assertEquals(0, game.boardList.size());
		game.addObjects();
		assertEquals(16, game.boardList.size());
	}

	@Test
	public void testDice() {
		// Couldn't find a way to test a random number generator
		// so here's half of the method working instead.
		
		// once the dice is rolled, return the same number every time.
		game.dice = 4;
		game.rolled = true;
		game.dice();
		assertEquals(4, game.dice);
		game.dice();
		assertEquals(4, game.dice);
		game.dice();
		assertEquals(4, game.dice);
		game.dice();
		assertEquals(4, game.dice);
	}

	@Test
	public void testMove() throws NumberFormatException {
		
		// if the dice roll extends past 100, you may enter anything 
		// between 100 and your dice roll - you will go to 100.
		game.rolled = true;
		game.pos = 98;
		game.dice = 6;		
		assertEquals(1, game.move("104"));
		assertEquals(100, game.pos);
		game.pos = 98;
		game.dice = 6;
		assertEquals(1, game.move("100"));
		assertEquals(100, game.pos);
		
		// a typical "correct" scenario.
		game.pos = 1;
		game.dice = 5;
		assertEquals(1, game.move("6"));
		assertEquals(6, game.pos);
		assertEquals(false, game.rolled);
		
		// cannot move if dice hasn't been rolled.
		assertEquals(2, game.move("6"));
		
		// cannot move if you do not enter player position + dice roll.
		game.pos = 50;
		game.dice = 3;
		game.rolled = true;
		assertEquals(3, game.move("56"));
		assertEquals(50, game.pos);
		assertEquals(3, game.dice);		
	}

	@Test
	public void testCheckTile() {
		// I wasn't sure how to do this one, since it relies on BoardObjects,
		// so I tested it as-is, instead of adding new objects.		
		
		// tile 54 contains a snake to tile 34.
		game.pos = 54;
		assertNull(game.tempBo);
		game.checkTile();
		assertEquals(game.boardList.get(9) ,game.tempBo); // store object in variable.
		assertEquals(34, game.pos);
		game.pos = 54;
		assertTrue(game.checkTile());	
	}

	@Test
	public void testSetTurn() {
		
		// save pos to current player, set pos to next player, set next 
		// player's position as pos, set turn number to next player number.
		game.p1 = 26;
		game.p2 = 79;
		game.pos = 58;
		game.turn = 1;

		assertEquals(26, game.p1);
		assertEquals(79, game.p2);
		assertEquals(58, game.pos);
		assertEquals(1, game.turn);

		game.setTurn();

		assertEquals(58, game.p1);
		assertEquals(79, game.p2);
		assertEquals(79, game.pos);
		assertEquals(2, game.turn);

		game.setTurn();

		assertEquals(58, game.p1);
		assertEquals(79, game.p2);
		assertEquals(58, game.pos);
		assertEquals(1, game.turn);
	}

	@Test
	public void testWinner() {
		assertEquals(0, game.winner());
		game.p1 = 100; 
		assertEquals(1, game.winner()); // test player 1 as winner
		game.p1 = 1;
		assertEquals(0, game.winner());// test no winner
		game.p2 = 100; 
		assertEquals(2, game.winner()); // test player 2 as winner
	}

	@Test
	public void testGetStats() {
		game.p1 = 25;
		game.p2 = 43;
		game.turn = 2;
		assertEquals("Player 1 is on tile 25" + System.getProperty("line.separator") + "Player 2 is on tile 43"
				+ System.getProperty("line.separator") + "It is Player 2's turn: ", game.getStats());
		game.p1 = 1;
		game.p2 = 92;
		game.turn = 1;
		assertEquals("Player 1 is on tile 1" + System.getProperty("line.separator") + "Player 2 is on tile 92"
				+ System.getProperty("line.separator") + "It is Player 1's turn: ", game.getStats());
	}

	@Test
	public void testSaveLoad() throws FileNotFoundException {
				
		game.p1 = 26; // save variables to file
		game.p2 = 94;
		game.pos = 56;
		game.turn = 2;
		game.dice = 4;
		game.rolled = true;

		game.save();

		game.p1 = 1; // reset variables
		game.p2 = 1;
		game.pos = 1;
		game.turn = 1;
		game.dice = 0;
		game.rolled = false;

		game.load(); 

		assertEquals(26, game.p1); // check the variables are the same that 
		assertEquals(94, game.p2); // were saved
		assertEquals(56, game.pos);
		assertEquals(2, game.turn);
		assertEquals(4, game.dice);
		assertTrue(game.rolled);

	}

	@Test
	public void testReset() {
		
		// non default variables
		game.p1 = 26;
		game.p2 = 94;
		game.pos = 56;
		game.turn = 2;
		game.dice = 4;
		game.rolled = true;
		
		game.reset();
		
		// default variables after reset
		assertEquals(1, game.p1); 
		assertEquals(1, game.p2);
		assertEquals(1, game.pos);
		assertEquals(1, game.turn);
		assertEquals(0, game.dice);
		assertFalse(game.rolled);

	}

}
