package engine;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * This class is the main engine. 
 * 
 * The game is started and controlled from the UI, which will reference 
 * the methods here to make the game run.
 * 
 * @author Jared Kinnear
 */

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Game {

	// Player positions and current player's turn
	int p1 = 1;
	int p2 = 1;
	public int turn = 1;

	// holds the current player's position
	// This needs to be public to accommodate the new GUI since my code is badly
	// designed
	public int pos = p1;

	// integer to hold die value
	// This needs to be public to accommodate the new GUI since my code is badly
	// designed
	public int dice;	
	
	// A seed because I can't get the fscking thing to work from the command line
	int seed = 1234;

	// integer to hold the last player/tile the dice was rolled on
	boolean rolled = false;

	// Board object that player has landed on, stored for convenience purposes
	// This needs to be public to accommodate the new GUI since my code is badly
	// designed
	public BoardObjects tempBo;

	// List to hold all board objects
	ArrayList<BoardObjects> boardList = new ArrayList<BoardObjects>();

	/**
	 * Create and add all objects to the board list.
	 * 
	 * Creates all required board objects, accepting start and end points as
	 * parameters and stores them in the boardList array.
	 */
	public void addObjects() {
		boardList.add(new BoardObjects.Ladder(1, 38));
		boardList.add(new BoardObjects.Ladder(4, 14));
		boardList.add(new BoardObjects.Ladder(9, 31));
		boardList.add(new BoardObjects.Ladder(21, 42));
		boardList.add(new BoardObjects.Ladder(28, 84));
		boardList.add(new BoardObjects.Ladder(51, 67));
		boardList.add(new BoardObjects.Ladder(71, 91));
		boardList.add(new BoardObjects.Ladder(80, 100));
		boardList.add(new BoardObjects.Snake(17, 7));
		boardList.add(new BoardObjects.Snake(54, 34));
		boardList.add(new BoardObjects.Snake(62, 19));
		boardList.add(new BoardObjects.Snake(64, 60));
		boardList.add(new BoardObjects.Snake(87, 24));
		boardList.add(new BoardObjects.Snake(93, 73));
		boardList.add(new BoardObjects.Snake(95, 75));
		boardList.add(new BoardObjects.Snake(98, 79));
	}

	/**
	 * If the dice has not been rolled this round (as set by Game.move()),
	 * generate a new number between 1 and 6 (inclusive), store it in the dice
	 * integer variable, set the rolled variable to true and return the current
	 * dice value.
	 * 
	 * If the dice has already been rolled this round, return the current dice
	 * value. (to prevent multiple dice attempts)
	 * 
	 * @return dice
	 */
	public int dice() {
		if (!rolled) {
			Random rand = new Random(seed);
			dice = rand.nextInt(6) + 1;
			seed++;
			rolled = true;
			return dice;
		}
		return dice;
	}

	/**
	 * This method is responsible for moving the player's token on the board.
	 * 
	 * Uses the rolled variable to check if the dice has been rolled this turn,
	 * if false, return 3.
	 * 
	 * Accepts the player's desired tile number as an input, the parameter must
	 * be equal to the number rolled by the dice plus the player's current
	 * position. If this is false, returns 2.
	 * 
	 * Additionally, if the player's position plus the dice roll are equal to or
	 * greater than 100, the player may choose any tile greater than or equal to
	 * 100 to go straight to tile 100.
	 * 
	 * If the previous conditions have been met, set the player's position to
	 * equal the parameter t, reset the rolled variable to false to prepare for
	 * next turn and return 1.
	 * 
	 * @param tile
	 *            number
	 * @return 0 - 3
	 */
	public int move(String t) throws NumberFormatException {
		int tile = Integer.parseInt(t);
		if (rolled == true && pos + dice >= 100 && tile >= 100) {
			pos = 100;
			return 1;
		} else if (rolled == true && tile == pos + dice) {
			pos = tile;
			rolled = false;
			return 1;
		} else if (rolled == false) {
			return 2;
		} else if (tile != pos + dice) {
			return 3;
		} else {
			return 0;
		}
	}

	/**
	 * Checks if player position contains an object.
	 * 
	 * If true, it will store the object in the tempBo variable and move the
	 * player to the object's end.
	 * 
	 * @return boolean
	 */
	public boolean checkTile() {
		for (BoardObjects p : boardList) {
			int i = p.getStart();
			if (i == pos) {
				tempBo = p;
				pos = p.getEnd();
				return true;
			}
		}
		return false;
	}

	/**
	 * Ends the current player's turn and begins the next.
	 * 
	 * Saves the current player's position and loads the next into the pos
	 * variable. Sets the turn variable to reflect the next player's turn.
	 */
	public void setTurn() {
		if (turn == 1) {
			p1 = pos;
			pos = p2;
			turn = 2;
		} else {
			p2 = pos;
			pos = p1;
			turn = 1;
		}
	}

	/**
	 * checks to see if a player has made it to the end tile.
	 * 
	 * Returns 1 for player 1, 2 for player 2 and 0 for none.
	 * 
	 * @return 0 - 2
	 */
	public int winner() {
		if (p1 == 100) {
			return 1;
		} else if (p2 == 100)
			return 2;
		else
			return 0;
	}

	/**
	 * Returns a message string with player's position.
	 * 
	 * @return String containing player's position
	 */
	public String getStats() {
		return "Player 1 is on tile " + p1 + System.getProperty("line.separator") + "Player 2 is on tile " + p2
				+ System.getProperty("line.separator") + "It is Player " + turn + "'s turn: ";

	}

	/**
	 * save progress to file
	 * 
	 * @throws FileNotFoundException
	 */
	public void save() throws FileNotFoundException {
		try (PrintWriter out = new PrintWriter("save.txt")) {
			out.printf(p1 + "\r\n" + p2 + "\r\n" + turn + "\r\n" + pos + "\r\n" + dice + "\r\n" + seed + "\r\n" + rolled);
		}
	}

	/**
	 * load progress from file
	 * 
	 * @throws FileNotFoundException
	 */
	public void load() throws FileNotFoundException {
		File save = new File("save.txt");
		Scanner input = new Scanner(save);
		ArrayList<Integer> loadList = new ArrayList<>();
		while (input.hasNextInt()) {
			loadList.add(input.nextInt());
		}
		rolled = input.nextBoolean();
		input.close();
		p1 = loadList.get(0);
		p2 = loadList.get(1);
		turn = loadList.get(2);
		pos = loadList.get(3);
		dice = loadList.get(4);
		seed = loadList.get(5);
	}

	/**
	 * reset the game board to the starting state.
	 */
	public void reset() {
		p1 = 1;
		p2 = 1;
		pos = 1;
		turn = 1;
		dice = 0;
		seed = 1234;
		rolled = false;		
	}
}
