package engine;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class TextUI {

	/**
	 * Reads commands from the given input, line by line. Relies on methods from
	 * the Game class and Prints feedback to the user through the text user
	 * interface.
	 *
	 * @param input
	 */
	public void play(Scanner in) {
		String newLine = System.getProperty("line.separator");
		Game game = new Game();
		game.addObjects();

		System.out.println("Welcome to Snakes and Ladders! first player to reach tile 100 wins!" + newLine);
		System.out.println("Ladders will give you a shortcut to a further tile, but watch out for snakes!");
		System.out.println("if they eat you, you will be sent backwards!" + newLine);
		System.out.println("The valid commands are: Help, ?, roll, move to <tile number>, save, load, reset, quit.");

		while (game.winner() == 0) {
			System.out.print(newLine + game.getStats());
			String line = in.nextLine().toLowerCase().trim();
			if (line.equals("help") || line.equals("?")) {
				System.out.println("The valid commands are: [help], [?], [save], [load], [roll], [reset], [quit]");
				System.out.println("and [move to <tile number>] (do not include the brackets)");

			} else if (line.startsWith("move to ")) {
				try {
					int moveResult = game.move(line.substring(8));
					if (moveResult == 3) {
						System.err.println("You must move to the tile that equals your tile + the dice " + "( "
								+ game.pos + " + " + game.dice + " = " + "?" + " )" + newLine);
					} else if (moveResult == 2) {
						System.err.println("You must roll the dice first!" + newLine);
					} else if (moveResult == 1) {
						if (game.checkTile()) {
							System.out.println("You hit a " + game.tempBo.getType() + " and go to tile "
									+ game.tempBo.getEnd() + "!");
						}
						game.setTurn();
					} else if (moveResult == 0) {
						System.err.println("Unkown error");
					}
				} catch (NumberFormatException e) {
					System.err.println("You must enter a number instead of" + e.getMessage().substring(16));
				}

			} else if (line.equals("roll")) {
				int dice = game.dice();
				System.out.println("You roll the dice and get a " + dice);

			} else if (line.equals("save")) {
				try {
					game.save();
					System.out.println("You have saved the game");
				} catch (FileNotFoundException e) {
					System.err.println(newLine + "File not found, check permissions and path: " + e.getMessage());
				}

			} else if (line.equals("load")) {
				try {
					game.load();
				} catch (FileNotFoundException e) {
					System.err.println(newLine + "File not found, check permissions and path: " + e.getMessage());
				}
				System.out.println("You have loaded the game from the last save");

			} else if (line.equals("reset")) {
				game.reset();
				System.out.println("You have reset the game");

			} else if (line.equals("quit")) {
				System.out.println("You have quit the game." + newLine + "Game Over");
				break;

			} else {
				System.out.println("That is not a valid command, type ? or help for a list of commands.");
			}
		}
		if (game.winner() != 0)
			System.out.println(
					newLine + "Player " + game.winner() + " has reached tile 100 and wins!" + newLine + "Game Over");
	}

	public static void main(String[] args) {		
		TextUI Game = new TextUI();
		Game.play(new Scanner(System.in));
	}

}
